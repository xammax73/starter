# starter

Welcome to jooby starter!!

## running

    ./gradlew joobyRun

## building

    ./gradlew build

## docker

     docker build . -t starter
     docker run -p 8080:8080 -it starter
