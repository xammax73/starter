package starter.hibernate;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jooby.Jooby;
import io.jooby.di.GuiceModule;
import io.jooby.flyway.FlywayModule;
import io.jooby.hibernate.HibernateModule;
import io.jooby.hikari.HikariModule;
import io.jooby.json.JacksonModule;
import io.xammax.jooby.hibernate.injector.HibernateExtension;
import starter.hibernate.injector.AppExtension;
import starter.hibernate.route.CityRoute;
import starter.hibernate.route.CountryRoute;
import starter.hibernate.route.HotelRoute;
import starter.hibernate.route.SettingRoute;

import static io.jooby.ExecutionMode.EVENT_LOOP;

public class App extends Jooby {

  {
    /* JSON */
    install(new JacksonModule());
    ObjectMapper mapper = require(ObjectMapper.class);
    mapper.findAndRegisterModules();

    /* Hikari */
    install(new HikariModule("db.main"));

    /* Hibernate */
    install(new HibernateModule("db.main")
        .scan("starter.hibernate.model.entity")
    );

    /* Flyway */
    install(new FlywayModule("db.main"));

    /* Guice */
    install(new GuiceModule());

    /* Extensions */
    install(new HibernateExtension());
    install(new AppExtension());

    /* Routes */
    mount("/api/settings", new SettingRoute());
    mount("/api/countries", new CountryRoute());
    mount("/api/cities", new CityRoute());
    mount("/api/hotels", new HotelRoute());

    get("/", ctx -> "Welcome to Jooby!");
  }

  public static void main(final String[] args) {
    runApp(args, EVENT_LOOP, App::new);
  }

}
