package starter.hibernate.injector;

import com.google.inject.Guice;
import com.google.inject.Injector;
import io.jooby.Extension;
import io.jooby.Jooby;
import io.jooby.ServiceRegistry;
import starter.hibernate.service.CityService;
import starter.hibernate.service.CountryService;
import starter.hibernate.service.HotelService;
import starter.hibernate.service.SettingService;

import javax.annotation.Nonnull;

import static io.xammax.jooby.JoobyInstance.app;

public class AppExtension implements Extension {

  public void install(@Nonnull Jooby application) {
    /* registry */
    ServiceRegistry registry = app.getServices();
    Injector injector = Guice.createInjector(new AppModule());
    /* repository
    SettingRepository settingRepository = injector.getInstance(SettingRepository.class);
    registry.put(SettingRepository.class, settingRepository);
    CountryRepository countryRepository = injector.getInstance(CountryRepository.class);
    registry.put(CountryRepository.class, countryRepository);
    CityRepository cityRepository = injector.getInstance(CityRepository.class);
    registry.put(CityRepository.class, cityRepository);
    HotelRepository hotelRepository = injector.getInstance(HotelRepository.class);
    registry.put(HotelRepository.class, hotelRepository);
    /* service */
    SettingService settingService = injector.getInstance(SettingService.class);
    registry.put(SettingService.class, settingService);
    CountryService countryService = injector.getInstance(CountryService.class);
    registry.put(CountryService.class, countryService);
    CityService cityService = injector.getInstance(CityService.class);
    registry.put(CityService.class, cityService);
    HotelService hotelService = injector.getInstance(HotelService.class);
    registry.put(HotelService.class, hotelService);
  }

}