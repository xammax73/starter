package starter.hibernate.injector;

import com.google.inject.AbstractModule;
import starter.hibernate.repository.*;

public class AppModule extends AbstractModule {

  @Override
  protected void configure() {
    bind(SettingRepository.class).to(SettingRepositoryImpl.class);
    bind(CountryRepository.class).to(CountryRepositoryImpl.class);
    bind(CityRepository.class).to(CityRepositoryImpl.class);
    bind(HotelRepository.class).to(HotelRepositoryImpl.class);
  }

}
