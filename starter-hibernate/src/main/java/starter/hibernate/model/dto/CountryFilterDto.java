package starter.hibernate.model.dto;

import java.util.Map;

public class CountryFilterDto extends CountryDto {

  private Map<String, String> sortFields;

  public CountryFilterDto() {
    super();
  }

  public CountryFilterDto(Map<String, String> sortFields) {
    super();
    this.sortFields = sortFields;
  }

  public Map<String, String> getSortFields() {
    return this.sortFields;
  }

  public void setSortFields(Map<String, String> sortFields) {
    this.sortFields = sortFields;
  }

}
