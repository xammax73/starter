package starter.hibernate.model.dto;

public class SettingDto {

  protected Long id;
  private String name;
  private String alias;
  private String description;
  private String type;
  private String value;
  private Long position;

  public SettingDto() { }

  public SettingDto(
      Long id,
      String name,
      String alias,
      String description,
      String type,
      String value,
      Long position
  ) {
    this.id = id;
    this.name = name;
    this.alias = alias;
    this.description = description;
    this.type = type;
    this.value = value;
    this.position = position;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAlias() {
    return this.alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getType() {
    return this.type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getValue() {
    return this.value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public Long getPosition() {
    return this.position;
  }

  public void setPosition(Long position) {
    this.position = position;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof SettingDto)) return false;
    return id != null && id.equals(((SettingDto) o).getId()) &&
        name != null && name.equals(((SettingDto) o).getName()) &&
        alias != null && alias.equals(((SettingDto) o).getAlias());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "{" +
        (getId() != null ? "id:" + getId().toString() : "") +
        (getId() != null && (getName() != null || getAlias() != null || getDescription() != null || getType() != null || getValue() != null || getPosition() != null) ? "," : "") +
        (getName() != null ? "name:" + getName() : "") +
        (getName() != null && (getAlias() != null || getDescription() != null || getType() != null || getValue() != null || getPosition() != null) ? "," : "") +
        (getAlias() != null ? "alias:" + getAlias() : "") +
        (getAlias() != null && (getDescription() != null || getType() != null || getValue() != null || getPosition() != null) ? "," : "") +
        (getDescription() != null ? "description:" + getDescription() : "") +
        (getDescription() != null && (getType() != null || getValue() != null || getPosition() != null) ? "," : "") +
        (getType() != null ? "type:" + getType() : "") +
        (getType() != null && (getValue() != null || getPosition() != null) ? "," : "") +
        (getValue() != null ? "value:" + getValue() : "") +
        (getValue() != null && getPosition() != null ? "," : "") +
        (getPosition() != null ? "position:" + getPosition().toString() : "") +
        "}";
  }

}
