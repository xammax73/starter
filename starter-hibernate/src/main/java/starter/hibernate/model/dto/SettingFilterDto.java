package starter.hibernate.model.dto;

import java.util.Map;

public class SettingFilterDto extends SettingDto {

  private Map<String, String> sortFields;

  public SettingFilterDto() {
    super();
  }

  public SettingFilterDto(Map<String, String> sortFields) {
    super();
    this.sortFields = sortFields;
  }

  public Map<String, String> getSortFields() {
    return this.sortFields;
  }

  public void setSortFields(Map<String, String> sortFields) {
    this.sortFields = sortFields;
  }

}
