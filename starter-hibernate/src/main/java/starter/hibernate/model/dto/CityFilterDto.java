package starter.hibernate.model.dto;

import java.util.Map;

public class CityFilterDto extends CityDto {

  private Map<String, String> sortFields;

  public CityFilterDto() {
    super();
  }

  public CityFilterDto(Map<String, String> sortFields) {
    super();
    this.sortFields = sortFields;
  }

  public Map<String, String> getSortFields() {
    return this.sortFields;
  }

  public void setSortFields(Map<String, String> sortFields) {
    this.sortFields = sortFields;
  }

}
