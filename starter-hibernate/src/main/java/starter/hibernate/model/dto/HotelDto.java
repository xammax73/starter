package starter.hibernate.model.dto;

public class HotelDto {

  private Long id;
  private Long cityId;
  private String name;
  private String address;
  private String postalCode;
  private String hotelClass;
  private Integer price;
  private Boolean enabled;
  private Long position;

  public HotelDto() { }

  public HotelDto(
      Long id,
      Long cityId,
      String name,
      String address,
      String postalCode,
      String hotelClass,
      Integer price,
      Boolean enabled,
      Long position) {
    this.id = id;
    this.cityId = cityId;
    this.name = name;
    this.address = address;
    this.postalCode = postalCode;
    this.hotelClass = hotelClass;
    this.price = price;
    this.enabled = enabled;
    this.position = position;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getCityId() {
    return this.cityId;
  }

  public void setCityId(Long cityId) {
    this.cityId = cityId;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return this.address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPostalCode() {
    return this.postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public String getHotelClass() {
    return this.hotelClass;
  }

  public void setHotelClass(String hotelClass) {
    this.hotelClass = hotelClass;
  }

  public Integer getPrice() {
    return this.price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public Boolean isEnabled() {
    return this.enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public Long getPosition() {
    return this.position;
  }

  public void setPosition(Long position) {
    this.position = position;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof HotelDto)) return false;
    return id != null && id.equals(((HotelDto) o).getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "{" +
        (getId() != null ? "id:" + getId().toString() : "") +
        (getId() != null && (getCityId() != null || getName() != null || getAddress() != null || getPostalCode() != null || getHotelClass() != null || getPrice() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getCityId() != null ? "cityId:" + getCityId().toString() : "") +
        (getCityId() != null && (getName() != null || getAddress() != null || getPostalCode() != null || getHotelClass() != null || getPrice() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getName() != null ? "name:" + getName() : "") +
        (getName() != null && (getAddress() != null || getPostalCode() != null || getHotelClass() != null || getPrice() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getAddress() != null ? "address:" + getAddress() : "") +
        (getAddress() != null && (getPostalCode() != null || getHotelClass() != null || getPrice() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getPostalCode() != null ? "postalCode:" + getPostalCode() : "") +
        (getPostalCode() != null && (getHotelClass() != null || getPrice() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getHotelClass() != null ? "hotelClass:" + getHotelClass() : "") +
        (getHotelClass() != null && (getPrice() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getPrice() != null ? "price:" + getPrice().toString() : "") +
        (getPrice() != null && (isEnabled() != null || getPosition() != null) ? "," : "") +
        (isEnabled() != null ? "enabled:" + isEnabled() : "") +
        (isEnabled() != null && getPosition() != null ? "," : "") +
        (getPosition() != null ? "position:" + getPosition().toString() : "") +
        "}";
  }

}
