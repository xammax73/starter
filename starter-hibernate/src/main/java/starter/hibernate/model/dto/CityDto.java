package starter.hibernate.model.dto;

public class CityDto {

  private Long id;
  private Long countryId;
  private String name;
  private Boolean enabled;
  private Long position;

  public CityDto() { }

  public CityDto(
      Long id,
      Long countryId,
      String name,
      Boolean enabled,
      Long position) {
    this.id = id;
    this.countryId = countryId;
    this.name = name;
    this.enabled = enabled;
    this.position = position;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getCountryId() {
    return this.countryId;
  }

  public void setCountryId(Long countryId) {
    this.countryId = countryId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Boolean isEnabled() {
    return this.enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public Long getPosition() {
    return this.position;
  }

  public void setPosition(Long position) {
    this.position = position;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof CityDto)) return false;
    return id != null && id.equals(((CityDto) o).getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "{" +
        (getId() != null ? "id:" + getId().toString() : "") +
        (getId() != null && (getCountryId() != null || getName() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getCountryId() != null ? "countryId:" + getCountryId().toString() : "") +
        (getCountryId() != null && (getName() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getName() != null ? "name:" + getName() : "") +
        (getName() != null && (isEnabled() != null || getPosition() != null) ? "," : "") +
        (isEnabled() != null ? ",enabled:" + isEnabled() : "") +
        (isEnabled() != null && getPosition() != null ? "," : "") +
        (getPosition() != null ? ",position:" + getPosition().toString() : "") +
        "}";
  }

}
