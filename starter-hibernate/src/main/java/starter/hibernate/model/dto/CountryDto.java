package starter.hibernate.model.dto;

public class CountryDto {

  private Long id;
  private String code;
  private String name;
  private Boolean enabled;
  private Long position;

  public CountryDto() { }

  public CountryDto(
      Long id,
      String code,
      String name,
      Boolean enabled,
      Long position) {
    this.id = id;
    this.code = code;
    this.name = name;
    this.enabled = enabled;
    this.position = position;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Boolean isEnabled() {
    return this.enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public Long getPosition() {
    return this.position;
  }

  public void setPosition(Long position) {
    this.position = position;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof CountryDto)) return false;
    return id != null && id.equals(((CountryDto) o).getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "{" +
        (getId() != null ? "id:" + getId().toString() : "") +
        (getId() != null && (getCode() != null || getName() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getCode() != null ? ",code:" + getCode() : "") +
        (getCode() != null && (getName() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getName() != null ? "name:" + getName() : "") +
        (getName() != null && (isEnabled() != null || getPosition() != null) ? "," : "") +
        (isEnabled() != null ? "enabled:" + isEnabled() : "") +
        (isEnabled() != null && getPosition() != null ? "," : "") +
        (getPosition() != null ? "position:" + getPosition().toString() : "") +
        "}";
  }

}
