package starter.hibernate.model.entity;

import com.google.common.base.MoreObjects;
import io.xammax.jooby.hibernate.model.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "Country")
@Table(name = "starter_countries")
public class Country extends AbstractPersistable<Long> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false, unique = true, updatable = false)
  protected Long id;

  @Column(name = "code", length = 8, nullable = false, unique = true)
  private String code;

  @Column(name = "name", nullable = false, unique = true)
  private String name;

  @Column(name = "enabled", nullable = false)
  private Boolean enabled;

  @Column(name = "position", nullable = false)
  private Long position;

  public Country() {
    super();
  }

  public Country(String code, String name, Boolean enabled, Long position) {
    super();
    this.code = code;
    this.name = name;
    this.enabled = enabled;
    this.position = position;
  }

  @Override
  public Long getId() {
    return this.id;
  }

  public String getCode() {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Boolean isEnabled() {
    return this.enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public Long getPosition() {
    return this.position;
  }

  public void setPosition(Long position) {
    this.position = position;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Country)) return false;
    return id != null && id.equals(((Country) o).getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "{" +
        (getId() != null ? MoreObjects.toStringHelper(this).add("id", getId().toString()).toString() : "") +
        (getId() != null && (getCode() != null || getName() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getCode() != null ? MoreObjects.toStringHelper(this).add("code", getCode()).toString() : "") +
        (getCode() != null && (getName() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getName() != null ? MoreObjects.toStringHelper(this).add("name", getName()).toString() : "") +
        (getName() != null && (isEnabled() != null || getPosition() != null) ? "," : "") +
        (isEnabled() != null ? MoreObjects.toStringHelper(this).add("enabled", isEnabled().toString()).toString() : "") +
        (isEnabled() != null && getPosition() != null ? "," : "") +
        (getPosition() != null ? MoreObjects.toStringHelper(this).add("position", getPosition().toString()).toString() : "") +
        "}";
  }

}