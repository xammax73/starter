package starter.hibernate.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.google.common.base.MoreObjects;
import io.xammax.jooby.hibernate.model.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "City")
@Table(name = "starter_cities")
public class City extends AbstractPersistable<Long> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false, unique = true, updatable = false)
  protected Long id;

  @Column(name = "name", nullable = false, unique = true)
  private String name;

  @Column(name = "enabled", nullable = false)
  private Boolean enabled;

  @Column(name = "position", nullable = false)
  private Long position;

  @ManyToOne(targetEntity = Country.class, fetch = FetchType.LAZY)
  @JoinColumn(name = "country_id")
  @JsonBackReference
  private Country country;

  public City() {
    super();
  }

  public City(
      String name,
      Boolean enabled,
      Long position,
      Country country) {
    super();
    this.name = name;
    this.enabled = enabled;
    this.position = position;
    this.country = country;
  }

  @Override
  public Long getId() {
    return this.id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Boolean isEnabled() {
    return this.enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public Long getPosition() {
    return this.position;
  }

  public void setPosition(Long position) {
    this.position = position;
  }

  public Country getCountry() {
    return this.country;
  }

  public void setCountry(Country country) {
    this.country = country;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof City)) return false;
    return id != null && id.equals(((City) o).getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "{" +
        (getId() != null ? MoreObjects.toStringHelper(this).add("id", getId().toString()).toString() : "") +
        (getId() != null && (getCountry() != null || getName() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getCountry() != null ? MoreObjects.toStringHelper(this).add("country", getCountry().getId()).toString() : "") +
        (getCountry() != null && (getName() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getName() != null ? MoreObjects.toStringHelper(this).add("name", getName()).toString() : "") +
        (getName() != null && (isEnabled() != null || getPosition() != null) ? "," : "") +
        (isEnabled() != null ? MoreObjects.toStringHelper(this).add("enabled", isEnabled().toString()).toString() : "") +
        (isEnabled() != null && getPosition() != null ? "," : "") +
        (getPosition() != null ? MoreObjects.toStringHelper(this).add("position", getPosition().toString()).toString() : "") +
        "}";
  }

}