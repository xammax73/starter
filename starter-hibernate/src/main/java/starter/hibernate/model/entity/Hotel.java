package starter.hibernate.model.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.google.common.base.MoreObjects;
import io.xammax.jooby.hibernate.model.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "Hotel")
@Table(name = "starter_hotels")
public class Hotel extends AbstractPersistable<Long> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false, unique = true, updatable = false)
  protected Long id;

  @Column(name = "name", nullable = false, unique = true)
  private String name;

  @Column(name = "address", nullable = false, unique = true)
  private String address;

  @Column(name = "postal_code", nullable = false)
  private String postalCode;

  @Column(name = "hotel_class", nullable = false)
  private String hotelClass;

  @Column(name = "price", nullable = false)
  private Integer price;

  @Column(name = "enabled", nullable = false)
  private Boolean enabled;

  @Column(name = "position", nullable = false)
  private Long position;

  @ManyToOne(targetEntity = City.class, fetch = FetchType.LAZY)
  @JoinColumn(name = "city_id")
  @JsonBackReference
  private City city;

  public Hotel() {
    super();
  }

  public Hotel(
      String name,
      String address,
      String postalCode,
      String hotelClass,
      Integer price,
      Boolean enabled,
      Long position,
      City city) {
    super();
    this.name = name;
    this.address = address;
    this.postalCode = postalCode;
    this.hotelClass = hotelClass;
    this.price = price;
    this.enabled = enabled;
    this.position = position;
    this.city = city;
  }

  @Override
  public Long getId() {
    return this.id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAddress() {
    return this.address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPostalCode() {
    return this.postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public String getHotelClass() {
    return this.hotelClass;
  }

  public void setHotelClass(String hotelClass) {
    this.hotelClass = hotelClass;
  }

  public Integer getPrice() {
    return this.price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public Boolean isEnabled() {
    return this.enabled;
  }

  public void setEnabled(Boolean enabled) {
    this.enabled = enabled;
  }

  public Long getPosition() {
    return this.position;
  }

  public void setPosition(Long position) {
    this.position = position;
  }

  public City getCity() {
    return this.city;
  }

  public void setCity(City city) {
    this.city = city;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Hotel)) return false;
    return id != null && id.equals(((Hotel) o).getId());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "{" +
        (getId() != null ? MoreObjects.toStringHelper(this).add("id", getId().toString()).toString() : "") +
        (getId() != null && (getCity() != null || getName() != null || getAddress() != null || getPostalCode() != null || getHotelClass() != null || getPrice() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getCity() != null ? MoreObjects.toStringHelper(this).add("city", getCity().getId()).toString() : "") +
        (getCity() != null && (getName() != null || getAddress() != null || getPostalCode() != null || getHotelClass() != null || getPrice() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getName() != null ? MoreObjects.toStringHelper(this).add("name", getName()).toString() : "") +
        (getName() != null && (getAddress() != null || getPostalCode() != null || getHotelClass() != null || getPrice() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getAddress() != null ? MoreObjects.toStringHelper(this).add("address", getAddress()).toString() : "") +
        (getAddress() != null && (getPostalCode() != null || getHotelClass() != null || getPrice() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getPostalCode() != null ? MoreObjects.toStringHelper(this).add("postalCode", getPostalCode()).toString() : "") +
        (getPostalCode() != null && (getHotelClass() != null || getPrice() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getHotelClass() != null ? MoreObjects.toStringHelper(this).add("hotelClass", getHotelClass()).toString() : "") +
        (getHotelClass() != null && (getPrice() != null || isEnabled() != null || getPosition() != null) ? "," : "") +
        (getPrice() != null ? MoreObjects.toStringHelper(this).add("price", getPrice().toString()).toString() : "") +
        (getPrice() != null && (isEnabled() != null || getPosition() != null) ? "," : "") +
        (isEnabled() != null ? MoreObjects.toStringHelper(this).add("enabled", isEnabled().toString()).toString() : "") +
        (isEnabled() != null && getPosition() != null ? "," : "") +
        (getPosition() != null ? MoreObjects.toStringHelper(this).add("position", getPosition().toString()).toString() : "") +
        "}";
  }

}
