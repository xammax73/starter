package starter.hibernate.model.entity;

import com.google.common.base.MoreObjects;
import io.xammax.jooby.hibernate.model.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "Setting")
@Table(name = "starter_settings")
public class Setting extends AbstractPersistable<Long> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false, unique = true, updatable = false)
  protected Long id;

  @Column(name = "name", nullable = false, unique = true)
  private String name;

  @Column(name = "alias", nullable = false, unique = true)
  private String alias;

  @Column(name = "description", nullable = false)
  private String description;

  @Column(name = "type", nullable = false)
  private String type;

  @Column(name = "value", nullable = false)
  private String value;

  @Column(name = "position", nullable = false)
  private Long position;

  public Setting() {
    super();
  }

  public Setting(
      String name,
      String alias,
      String description,
      String type,
      String value,
      Long position) {
    super();
    this.name = name;
    this.alias = alias;
    this.description = description;
    this.type = type;
    this.value = value;
    this.position = position;
  }

  public Long getId() {
    return this.id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getAlias() {
    return this.alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getType() {
    return this.type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getValue() {
    return this.value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public Long getPosition() {
    return this.position;
  }

  public void setPosition(Long position) {
    this.position = position;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Setting)) return false;
    return id != null && id.equals(((Setting) o).getId()) &&
        name != null && name.equals(((Setting) o).getName()) &&
        alias != null && alias.equals(((Setting) o).getAlias());
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + "{" +
        (getId() != null ? MoreObjects.toStringHelper(this).add("id", getId().toString()).toString() : "") +
        (getId() != null && (getName() != null || getAlias() != null || getDescription() != null || getType() != null || getValue() != null || getPosition() != null) ? "," : "") +
        (getName() != null ? MoreObjects.toStringHelper(this).add("name", getName()).toString() : "") +
        (getName() != null && (getAlias() != null || getDescription() != null || getType() != null || getValue() != null || getPosition() != null) ? "," : "") +
        (getAlias() != null ? "," + MoreObjects.toStringHelper(this).add("alias", getAlias()).toString() : "") +
        (getAlias() != null && (getDescription() != null || getType() != null || getValue() != null || getPosition() != null) ? "," : "") +
        (getDescription() != null ? "," + MoreObjects.toStringHelper(this).add("description", getDescription()).toString() : "") +
        (getDescription() != null && (getType() != null || getValue() != null || getPosition() != null) ? "," : "") +
        (getType() != null ? "," + MoreObjects.toStringHelper(this).add("type", getType()).toString() : "") +
        (getType() != null && (getValue() != null || getPosition() != null) ? "," : "") +
        (getValue() != null ? "," + MoreObjects.toStringHelper(this).add("value", getValue()).toString() : "") +
        (getValue() != null && getPosition() != null ? "," : "") +
        (getPosition() != null ? MoreObjects.toStringHelper(this).add("position", getPosition().toString()).toString() : "") +
        "}";
  }

}
