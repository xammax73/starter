package starter.hibernate.service;

import io.xammax.jooby.internal.hibernate.Direction;
import io.xammax.jooby.internal.hibernate.Search;
import io.xammax.jooby.internal.hibernate.Sort;
import starter.hibernate.model.dto.CityDto;
import starter.hibernate.model.dto.CityFilterDto;
import starter.hibernate.model.entity.City;
import starter.hibernate.model.entity.Country;
import starter.hibernate.repository.CityRepository;
import starter.hibernate.repository.CountryRepository;
import starter.hibernate.repository.SettingRepository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class CityService {

  private final CityRepository cityRepository;
  private final CountryRepository countryRepository;
  private final SettingRepository settingRepository;

  @Inject
  public CityService(
      CityRepository cityRepository,
      CountryRepository countryRepository,
      SettingRepository settingRepository) {
    this.cityRepository = cityRepository;
    this.countryRepository = countryRepository;
    this.settingRepository = settingRepository;
  }

  public CityRepository getCityRepository() {
    return cityRepository;
  }

  public CountryRepository getCountryRepository() {
    return countryRepository;
  }

  public SettingRepository getSettingRepository() {
    return settingRepository;
  }

  // Method getAll

  public Map<String, List<CityDto>> getAll(
      @Nullable Integer page, @Nullable Integer size, @Nullable String field, @Nullable String direction) {
    page = (page == null) ? 1 : page;
    size = (size == null) ? getSettingRepository().getMaxItemsPerPage() : size;
    field = ((field == null) || field.equals("")) ? "id" : field;
    direction = (direction == null) ? getSettingRepository().getSortDirection() : direction;
    Map<String, String> sortFields = new HashMap<>();
    sortFields.put(field, direction);
    CityFilterDto filterDto = new CityFilterDto();
    filterDto.setSortFields(sortFields);
    return getByFilter(filterDto, page, size);
  }

  // Method getById

  public Map<String, CityDto> getById(Long id) {
    Map<String, CityDto> response = new HashMap<>();
    Optional<City> optionalItem = getCityRepository().findById(id);

    // checking input parameters
    if (optionalItem.isEmpty()) {
      response.put("City with id = " + id + " not found", new CityDto());
      return response;
    }

    City city = optionalItem.get();
    CityDto cityDto = createDto(city);
    response.put("City with id = " + id + " found", cityDto);
    return response;
  }

  // Method create

  public Map<String, CityDto> create(CityDto cityDto) {
    Map<String, CityDto> response = new HashMap<>();

    // checking input parameters
    if (cityDto == null) {
      response.put("City form data is null", new CityDto());
      return response;
    }

    // checking unique empty fields
    if (cityDto.getName() == null || cityDto.getName().isEmpty()) {
      response.put("City name is not specified", cityDto);
      return response;
    }

    // checking unique fields
    Search search = new Search(Country.class);
    if (cityDto.getName() != null && !cityDto.getName().isEmpty()) {
      search.addFilterEqual("name", cityDto.getName());
      if (getCityRepository().searchUnique(search) != null) {
        response.put("\"City with name = " + cityDto.getName() + " already exists\"", cityDto);
        return response;
      }
    }

    // create with fields
    City city = createEntity(cityDto);
    city = getCityRepository().save(city);
    response.put("Country created successfully", updateDto(city, cityDto));
    return response;
  }

  // Method update

  public Map<String, CityDto> update(CityDto cityDto, Long id) {
    Map<String, CityDto> response = new HashMap<>();
    Optional<City> optionalItem = getCityRepository().findById(id);

    // checking input parameters
    if (cityDto == null) {
      response.put("City form data is null", new CityDto());
      return response;
    }

    if (optionalItem.isEmpty()) {
      response.put("\"City with id = " + id + " not found\"", cityDto);
      return response;
    }

    // checking unique fields
    City city = optionalItem.get();
    Search search = new Search(City.class);

    if (cityDto.getName() != null && !cityDto.getName().isEmpty()) {
      search.addFilterEqual("name", cityDto.getName());
      if (!city.equals(getCityRepository().searchUnique(search))) {
        response.put("\"City with name = " + cityDto.getName() + " already exists\"", cityDto);
        return response;
      }
    }

    // update with values
    Country country = Optional.of(getCountryRepository().findById(cityDto.getCountryId()))
        .map(Optional::get)
        .orElse(null);
    if (cityDto.getCountryId() != null) { city.setCountry(country); }
    if (cityDto.getName() != null) { city.setName(cityDto.getName()); }
    if (cityDto.isEnabled() != null) { city.setEnabled(cityDto.isEnabled()); }
    if (cityDto.getPosition() != null) { city.setPosition(cityDto.getPosition()); }

    response.put("City with id = " + id + " updated", updateDto(getCityRepository().update(city), cityDto));
    return response;
  }

  // Method delete

  public Map<String, CityDto> delete(Long id) {
    Map<String, CityDto> response = new HashMap<>();
    Optional<City> optionalItem = getCityRepository().findById(id);

    if (optionalItem.isEmpty()) {
      response.put("City with id = " + id + " not found", new CityDto());
      return response;
    }

    City city = optionalItem.get();
    CityDto cityDto = createDto(city);
    getCityRepository().deleteById(id);
    response.put("City with id = " + id + " deleted", cityDto);
    return response;
  }

  // Method getByFilter

  private @Nonnull Map<String, List<CityDto>> getByFilter(
      @Nonnull CityFilterDto filterDto, @Nonnull Integer page, @Nonnull Integer size) {

    Map<String, List<CityDto>> response = new HashMap<>();
    Search search = new Search(City.class);

    // checking input parameters
    if (filterDto.getName() != null && !filterDto.getName().equals("")) {
      search.addFilterEqual("name", filterDto.getName());
    }

    if (filterDto.isEnabled() != null) {
      search.addFilterEqual("enabled", filterDto.isEnabled());
    }

    if (filterDto.getPosition() != null && filterDto.getPosition() != 0) {
      search.addFilterLessThan("position", filterDto.getPosition());
    }

    if (filterDto.getSortFields() != null && !filterDto.getSortFields().isEmpty() && filterDto.getSortFields().size() > 0) {
      filterDto.getSortFields().forEach((k, v) -> search.addSort(new Sort(k, Direction.fromString(v))));
    }

    long count = getCityRepository().count(search);

    // pagination
    if (size > 0) {
      search.setMaxResults(size);
      search.setPageNumber(page - 1);
    }

    // search
    List<City> cities = getCityRepository().search(search);
    List<CityDto> cityDtos = new ArrayList<>();

    if (cities.size() > 0) {
      cities.forEach(city -> {
        CityDto cityDto = createDto(city);
        cityDtos.add(cityDto);
      });
      response.put("\"Cities list, page " + page + " (total " + count + ")\"", cityDtos);
    } else {
      response.put("\"Cities empty list, page " + page + " (total " + count + ")\"", cityDtos);
    }

    return response;
  }

  private @Nonnull CityDto createDto(@Nonnull City city) {
    CityDto cityDto = new CityDto();
    return updateDto(city, cityDto);
  }

  private @Nonnull CityDto updateDto(@Nonnull City city, @Nonnull CityDto cityDto) {
    cityDto.setId(city.getId());
    cityDto.setCountryId(city.getCountry().getId());
    cityDto.setName(city.getName());
    cityDto.setEnabled(city.isEnabled());
    cityDto.setPosition(city.getPosition());
    return cityDto;
  }

  private @Nonnull City createEntity(@Nonnull CityDto cityDto) {
    City city = new City();
    return updateEntity(cityDto, city);
  }

  private @Nonnull City updateEntity(@Nonnull CityDto cityDto, @Nonnull City city) {
    Country country = Optional.of(getCountryRepository().findById(cityDto.getCountryId()))
        .map(Optional::get)
        .orElse(null);
    city.setCountry(country);
    city.setName(cityDto.getName());
    city.setEnabled(cityDto.isEnabled());
    city.setPosition(cityDto.getPosition());
    return city;
  }

}
