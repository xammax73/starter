package starter.hibernate.service;

import io.xammax.jooby.internal.hibernate.Direction;
import io.xammax.jooby.internal.hibernate.Search;
import io.xammax.jooby.internal.hibernate.Sort;
import starter.hibernate.model.dto.CountryDto;
import starter.hibernate.model.dto.CountryFilterDto;
import starter.hibernate.model.entity.Country;
import starter.hibernate.repository.CountryRepository;
import starter.hibernate.repository.SettingRepository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class CountryService {

  private final CountryRepository countryRepository;
  private final SettingRepository settingRepository;

  @Inject
  public CountryService(
      CountryRepository countryRepository, SettingRepository settingRepository) {
    this.countryRepository = countryRepository;
    this.settingRepository = settingRepository;
  }

  public CountryRepository getCountryRepository() {
    return countryRepository;
  }

  public SettingRepository getSettingRepository() {
    return settingRepository;
  }

  // Method getAll

  public Map<String, List<CountryDto>> getAll(
      @Nullable Integer page, @Nullable Integer size, @Nullable String field, @Nullable String direction) {
    page = (page == null) ? 1 : page;
    size = (size == null) ? getSettingRepository().getMaxItemsPerPage() : size;
    field = ((field == null) || field.equals("")) ? "id" : field;
    direction = (direction == null) ? getSettingRepository().getSortDirection() : direction;
    Map<String, String> sortFields = new HashMap<>();
    sortFields.put(field, direction);
    CountryFilterDto filterDto = new CountryFilterDto();
    filterDto.setSortFields(sortFields);
    return getByFilter(filterDto, page, size);
  }

  // Method getById

  public Map<String, CountryDto> getById(Long id) {
    Map<String, CountryDto> response = new HashMap<>();
    Optional<Country> optionalItem = getCountryRepository().findById(id);

    // checking input parameters
    if (optionalItem.isEmpty()) {
      response.put("Country with id = " + id + " not found", new CountryDto());
      return response;
    }

    Country country = optionalItem.get();
    CountryDto countryDto = createDto(country);
    response.put("Country with id = " + id + " found", countryDto);
    return response;
  }

  // Method create

  public Map<String, CountryDto> create(CountryDto countryDto) {
    Map<String, CountryDto> response = new HashMap<>();

    // checking input parameters
    if (countryDto == null) {
      response.put("Country form data is null", new CountryDto());
      return response;
    }

    // checking unique empty fields
    if (countryDto.getCode() == null || countryDto.getCode().isEmpty()) {
      response.put("Country code is not specified", countryDto);
      return response;
    }

    if (countryDto.getName() == null || countryDto.getName().isEmpty()) {
      response.put("Country name is not specified", countryDto);
      return response;
    }

    // checking unique fields
    Search search = new Search(Country.class);
    search.addFilterEqual("code", countryDto.getCode());
    if (getCountryRepository().count(search) > 0) {
      response.put("\"Country with code = " + countryDto.getCode() + " already exists\"", countryDto);
      return response;
    }

    search.clear();
    search.addFilterEqual("name", countryDto.getName());
    if (getCountryRepository().count(search) > 0) {
      response.put("\"Country with name = " + countryDto.getName() + " already exists\"", countryDto);
      return response;
    }

    // create with fields
    Country country = createEntity(countryDto);
    country = getCountryRepository().save(country);
    response.put("Country created successfully", updateDto(country, countryDto));
    return response;
  }

  // Method update

  public Map<String, CountryDto> update(CountryDto countryDto, Long id) {
    Map<String, CountryDto> response = new HashMap<>();
    Optional<Country> optionalItem = getCountryRepository().findById(id);

    // checking input parameters
    if (countryDto == null) {
      response.put("Country form data is null", new CountryDto());
      return response;
    }

    if (optionalItem.isEmpty()) {
      response.put("\"Country with id = " + id + " not found\"", countryDto);
      return response;
    }

    // checking unique fields
    Country country = optionalItem.get();
    Search search = new Search(Country.class);

    if (countryDto.getCode() != null && !countryDto.getCode().isEmpty()) {
      search.addFilterEqual("code", countryDto.getCode());
      if (!country.equals(getCountryRepository().searchUnique(search))) {
        response.put("\"Country with code = " + countryDto.getCode() + " already exists\"", countryDto);
        return response;
      }
    }

    if (countryDto.getName() != null && !countryDto.getName().isEmpty()) {
      search.clear();
      search.addFilterEqual("name", countryDto.getName());
      if (!country.equals(getCountryRepository().searchUnique(search))) {
        response.put("\"Country with name = " + countryDto.getName() + " already exists\"", countryDto);
        return response;
      }
    }

    // update with values
    if (countryDto.getCode() != null) { country.setCode(countryDto.getCode()); }
    if (countryDto.getName() != null) { country.setName(countryDto.getName()); }
    if (countryDto.isEnabled() != null) { country.setEnabled(countryDto.isEnabled()); }
    if (countryDto.getPosition() != null) { country.setPosition(countryDto.getPosition()); }

    response.put("Country with id = " + id + " updated", updateDto(getCountryRepository().update(country), countryDto));
    return response;
  }

  // Method delete

  public Map<String, CountryDto> delete(Long id) {
    Map<String, CountryDto> response = new HashMap<>();
    Optional<Country> optionalItem = getCountryRepository().findById(id);

    if (optionalItem.isEmpty()) {
      response.put("Country with id = " + id + " not found", new CountryDto());
      return response;
    }

    Country country = optionalItem.get();
    CountryDto countryDto = createDto(country);
    getCountryRepository().deleteById(id);
    response.put("Country with id = " + id + " deleted", countryDto);
    return response;
  }

  // Method getByFilter

  private @Nonnull Map<String, List<CountryDto>> getByFilter(
      @Nonnull CountryFilterDto filterDto, @Nonnull Integer page, @Nonnull Integer size) {

    Map<String, List<CountryDto>> response = new HashMap<>();
    Search search = new Search(Country.class);

    // checking input parameters
    if (filterDto.getName() != null && !filterDto.getName().equals("")) {
      search.addFilterEqual("name", filterDto.getName());
    }

    if (filterDto.getCode() != null && !filterDto.getCode().equals("")) {
      search.addFilterEqual("code", filterDto.getCode());
    }

    if (filterDto.isEnabled() != null) {
      search.addFilterEqual("enabled", filterDto.isEnabled());
    }

    if (filterDto.getPosition() != null && filterDto.getPosition() != 0) {
      search.addFilterLessThan("position", filterDto.getPosition());
    }

    if (filterDto.getSortFields() != null && !filterDto.getSortFields().isEmpty() && filterDto.getSortFields().size() > 0) {
      filterDto.getSortFields().forEach((k, v) -> search.addSort(new Sort(k, Direction.fromString(v))));
    }

    long count = getCountryRepository().count(search);

    // pagination
    if (size > 0) {
      search.setMaxResults(size);
      search.setPageNumber(page - 1);
    }

    // search
    List<Country> countries = getCountryRepository().search(search);
    List<CountryDto> countryDtos = new ArrayList<>();

    if (countries.size() > 0) {
      countries.forEach(country -> {
        CountryDto countryDto = createDto(country);
        countryDtos.add(countryDto);
      });
      response.put("\"Countries list, page " + page + " (total " + count + ")\"", countryDtos);
    } else {
      response.put("\"Countries empty list, page " + page + " (total " + count + ")\"", countryDtos);
    }

    return response;
  }

  private @Nonnull CountryDto createDto(@Nonnull Country country) {
    CountryDto countryDto = new CountryDto();
    return updateDto(country, countryDto);
  }

  private @Nonnull CountryDto updateDto(@Nonnull Country country, @Nonnull CountryDto countryDto) {
    countryDto.setId(country.getId());
    countryDto.setName(country.getName());
    countryDto.setCode(country.getCode());
    countryDto.setEnabled(country.isEnabled());
    countryDto.setPosition(country.getPosition());
    return countryDto;
  }

  private @Nonnull Country createEntity(@Nonnull CountryDto countryDto) {
    Country country = new Country();
    return updateEntity(countryDto, country);
  }

  private @Nonnull Country updateEntity(@Nonnull CountryDto countryDto, @Nonnull Country country) {
    country.setName(countryDto.getName());
    country.setCode(countryDto.getCode());
    country.setEnabled(countryDto.isEnabled());
    country.setPosition(countryDto.getPosition());
    return country;
  }

}
