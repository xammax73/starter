package starter.hibernate.service;

import io.xammax.jooby.internal.hibernate.Direction;
import io.xammax.jooby.internal.hibernate.Search;
import io.xammax.jooby.internal.hibernate.Sort;
import starter.hibernate.model.dto.SettingDto;
import starter.hibernate.model.dto.SettingFilterDto;
import starter.hibernate.model.entity.Setting;
import starter.hibernate.repository.SettingRepository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class SettingService {

  private final SettingRepository settingRepository;

  @Inject
  public SettingService(
      SettingRepository settingRepository) {
    this.settingRepository = settingRepository;
  }

  public SettingRepository getSettingRepository() {
    return settingRepository;
  }

  // Method getAll

  public Map<String, List<SettingDto>> getAll(
      @Nullable Integer page, @Nullable Integer size, @Nullable String field, @Nullable String direction) {
    page = (page == null) ? 1 : page;
    size = (size == null) ? getSettingRepository().getMaxItemsPerPage() : size;
    field = ((field == null) || field.equals("")) ? "id" : field;
    direction = (direction == null) ? getSettingRepository().getSortDirection() : direction;
    Map<String, String> sortFields = new HashMap<>();
    sortFields.put(field, direction);
    SettingFilterDto filterDto = new SettingFilterDto();
    filterDto.setSortFields(sortFields);
    return getByFilter(filterDto, page, size);
  }

  // Method getById

  public Map<String, SettingDto> getById(Long id) {
    Map<String, SettingDto> response = new HashMap<>();
    Optional<Setting> optionalItem = getSettingRepository().findById(id);

    // checking input parameters
    if (optionalItem.isEmpty()) {
      response.put("\"Setting with id = " + id + " not found\"", new SettingDto());
      return response;
    }

    Setting setting = optionalItem.get();
    SettingDto settingDto = createDto(setting);
    response.put("\"Setting with id = " + id + " found\"", settingDto);
    return response;
  }

  // Method update

  public Map<String, SettingDto> update(SettingDto settingDto, Long id) {
    Map<String, SettingDto> response = new HashMap<>();
    Optional<Setting> optionalItem = getSettingRepository().findById(id);

    // Checking input parameters
    if (settingDto == null) {
      response.put("\"Setting form data is null\"", new SettingDto());
      return response;
    }

    if (optionalItem.isEmpty()) {
      response.put("\"Setting with id = " + id + " not found\"", settingDto);
      return response;
    }

    // Checking unique fields
    Setting setting = optionalItem.get();
    Search search = new Search(Setting.class);

    if (settingDto.getName() != null && !settingDto.getName().isEmpty()) {
      search.addFilterEqual("name", settingDto.getName());
      if (!setting.equals(getSettingRepository().searchUnique(search))) {
        response.put("\"Setting with name = " + settingDto.getName() + " already exists\"", settingDto);
        return response;
      }
    }

    if (settingDto.getAlias() != null && !settingDto.getAlias().isEmpty()) {
      search.clear();
      search.addFilterEqual("alias", settingDto.getAlias());
      if (!setting.equals(getSettingRepository().searchUnique(search))) {
        response.put("\"Setting with alias " + settingDto.getAlias() + " already exists\"", settingDto);
        return response;
      }
    }

    // Update with values
    if (settingDto.getAlias() != null) { setting.setName(settingDto.getAlias()); }
    if (settingDto.getName() != null) { setting.setName(settingDto.getName()); }
    if (settingDto.getDescription() != null) { setting.setDescription(settingDto.getDescription()); }
    if (settingDto.getType() != null) { setting.setType(settingDto.getType()); }
    if (settingDto.getValue() != null) { setting.setValue(settingDto.getValue()); }
    if (settingDto.getPosition() != null) { setting.setPosition(settingDto.getPosition()); }

    response.put("\"Setting with id = " + id + " updated successfully\"", updateDto(getSettingRepository().update(setting), settingDto));
    return response;
  }

  // Method getByFilter

  private @Nonnull Map<String, List<SettingDto>> getByFilter(
      @Nonnull SettingFilterDto filterDto, @Nonnull Integer page, @Nonnull Integer size) {

    Map<String, List<SettingDto>> response = new HashMap<>();
    Search search = new Search(Setting.class);

    // Checking input parameters
    if (filterDto.getName() != null && !filterDto.getName().equals("")) {
      search.addFilterEqual("name", filterDto.getName());
    }

    if (filterDto.getAlias() != null && !filterDto.getAlias().equals("")) {
      search.addFilterEqual("alias", filterDto.getAlias());
    }

    if (filterDto.getDescription() != null) {
      search.addFilterEqual("description", filterDto.getDescription());
    }

    if (filterDto.getType() != null) {
      search.addFilterEqual("type", filterDto.getType());
    }

    if (filterDto.getValue() != null) {
      search.addFilterEqual("value", filterDto.getValue());
    }

    if (filterDto.getPosition() != null && filterDto.getPosition() != 0) {
      search.addFilterLessThan("position", filterDto.getPosition());
    }

    if (filterDto.getSortFields() != null && !filterDto.getSortFields().isEmpty() && filterDto.getSortFields().size() > 0) {
      filterDto.getSortFields().forEach((k, v) -> search.addSort(new Sort(k, Direction.fromString(v))));
    }

    long count = getSettingRepository().count(search);

    // Pagination
    if (size > 0) {
      search.setMaxResults(size);
      search.setPageNumber(page - 1);
    }

    // Search
    List<Setting> settings = getSettingRepository().search(search);
    List<SettingDto> settingDtos = new ArrayList<>();

    if (settings.size() > 0) {
      settings.forEach(setting -> {
        SettingDto settingDto = createDto(setting);
        settingDtos.add(settingDto);
      });
      response.put("\"Settings list, page " + page + " (total " + count + ")\"", settingDtos);
    } else {
      response.put("\"Settings empty list, page " + page + " (total " + count + ")\"", settingDtos);
    }

    return response;
  }

  private @Nonnull SettingDto createDto(@Nonnull Setting setting) {
    SettingDto settingDto = new SettingDto();
    return updateDto(setting, settingDto);
  }

  private @Nonnull SettingDto updateDto(@Nonnull Setting setting, @Nonnull SettingDto settingDto) {
    settingDto.setId(setting.getId());
    settingDto.setName(setting.getName());
    settingDto.setAlias(setting.getAlias());
    settingDto.setDescription(setting.getDescription());
    settingDto.setType(setting.getType());
    settingDto.setValue(setting.getValue());
    settingDto.setPosition(setting.getPosition());
    return settingDto;
  }

  private @Nonnull Setting createEntity(@Nonnull SettingDto settingDto) {
    Setting setting = new Setting();
    return updateEntity(settingDto, setting);
  }

  private @Nonnull Setting updateEntity(@Nonnull SettingDto settingDto, @Nonnull Setting setting) {
    setting.setName(settingDto.getName());
    setting.setAlias(settingDto.getAlias());
    setting.setDescription(settingDto.getDescription());
    setting.setType(settingDto.getType());
    setting.setValue(settingDto.getValue());
    setting.setPosition(settingDto.getPosition());
    return setting;
  }

}
