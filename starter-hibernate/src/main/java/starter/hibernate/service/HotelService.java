package starter.hibernate.service;

import io.xammax.jooby.internal.hibernate.Direction;
import io.xammax.jooby.internal.hibernate.Search;
import io.xammax.jooby.internal.hibernate.Sort;
import starter.hibernate.model.dto.HotelDto;
import starter.hibernate.model.dto.HotelFilterDto;
import starter.hibernate.model.entity.City;
import starter.hibernate.model.entity.Hotel;
import starter.hibernate.repository.CityRepository;
import starter.hibernate.repository.HotelRepository;
import starter.hibernate.repository.SettingRepository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class HotelService {

  private final CityRepository cityRepository;
  private final HotelRepository hotelRepository;
  private final SettingRepository settingRepository;

  @Inject
  public HotelService(
      CityRepository cityRepository,
      HotelRepository hotelRepository,
      SettingRepository settingRepository) {
    this.cityRepository = cityRepository;
    this.hotelRepository = hotelRepository;
    this.settingRepository = settingRepository;
  }

  public CityRepository getCityRepository() {
    return cityRepository;
  }

  public HotelRepository getHotelRepository() {
    return hotelRepository;
  }

  public SettingRepository getSettingRepository() {
    return settingRepository;
  }

  // Method getAll

  public Map<String, List<HotelDto>> getAll(
      @Nullable Integer page, @Nullable Integer size, @Nullable String field, @Nullable String direction) {
    page = (page == null) ? 1 : page;
    size = (size == null) ? getSettingRepository().getMaxItemsPerPage() : size;
    field = ((field == null) || field.equals("")) ? "id" : field;
    direction = (direction == null) ? getSettingRepository().getSortDirection() : direction;
    Map<String, String> sortFields = new HashMap<>();
    sortFields.put(field, direction);
    HotelFilterDto filterDto = new HotelFilterDto();
    filterDto.setSortFields(sortFields);
    return getByFilter(filterDto, page, size);
  }

  public Map<String, HotelDto> getById(Long id) {
    Map<String, HotelDto> response = new HashMap<>();
    Optional<Hotel> optionalItem = getHotelRepository().findById(id);

    // checking input parameters
    if (optionalItem.isEmpty()) {
      response.put("Hotel with id = " + id + " not found", new HotelDto());
      return response;
    }

    Hotel hotel = optionalItem.get();
    HotelDto hotelDto = createDto(hotel);
    response.put("Hotel with id = " + id + " found", hotelDto);
    return response;
  }

  // Method create

  public Map<String, HotelDto> create(HotelDto hotelDto) {
    Map<String, HotelDto> response = new HashMap<>();

    // checking input parameters
    if (hotelDto == null) {
      response.put("Hotel form data is null", new HotelDto());
      return response;
    }

    // checking empty fields
    Search search = new Search(Hotel.class);
    City city;

    if (hotelDto.getCityId() == null || hotelDto.getCityId() == 0) {
      response.put("Hotel name is not specified", hotelDto);
      return response;
    } else {
      Optional<City> optionalCity = getCityRepository().findById(hotelDto.getCityId());

      if (optionalCity.isEmpty()) {
        response.put("Hotel name is not specified", hotelDto);
        return response;
      }

      city = optionalCity.get();
    }

    // checking empty fields
    if (hotelDto.getName() == null || hotelDto.getName().isEmpty()) {
      response.put("Hotel name is not specified", hotelDto);
      return response;
    }

    if (hotelDto.getAddress() == null || hotelDto.getAddress().isEmpty()) {
      response.put("Hotel address is not specified", hotelDto);
      return response;
    }

    if (hotelDto.getPostalCode() == null || hotelDto.getPostalCode().isEmpty()) {
      response.put("Hotel postal code is not specified", hotelDto);
      return response;
    }

    if (hotelDto.getHotelClass() == null || hotelDto.getHotelClass().isEmpty()) {
      response.put("Hotel class is not specified", hotelDto);
      return response;
    }

    if (hotelDto.getPrice() == null || hotelDto.getPrice() == 0) {
      response.put("Hotel apartment price is not specified", hotelDto);
      return response;
    }

    if (hotelDto.isEnabled() == null) {
      response.put("Hotel activity is not specified", hotelDto);
      return response;
    }

    // checking unique fields
    search.clear();
    search.addFilterEqual("name", hotelDto.getName());
    if (getHotelRepository().count(search) > 0) {
      response.put("\"Hotel with name = " + hotelDto.getName() + " already exists\"", hotelDto);
      return response;
    }

    search.clear();
    search.addFilterEqual("address", hotelDto.getAddress());
    if (getHotelRepository().count(search) > 0) {
      response.put("\"Hotel with address = " + hotelDto.getAddress() + " already exists\"", hotelDto);
      return response;
    }

    hotelDto.setPosition(hotelDto.getPosition() == null || hotelDto.getPosition() == 0
        ? getHotelRepository().count() + 1L
        : hotelDto.getPosition());

    // create with fields
    Hotel hotel = createEntity(hotelDto, city);
    hotel = getHotelRepository().save(hotel);
    response.put("Hotel with name = " + hotelDto.getName() + " created", updateDto(hotel, hotelDto));
    return response;
  }

  // Method update

  public Map<String, HotelDto> update(HotelDto hotelDto, Long id) {
    Map<String, HotelDto> response = new HashMap<>();
    Optional<Hotel> optionalItem = getHotelRepository().findById(id);

    // checking input parameters
    if (hotelDto == null) {
      response.put("Hotel form data is null", new HotelDto());
      return response;
    }

    if (optionalItem.isEmpty()) {
      response.put("Hotel with id = " + id + " not found", new HotelDto());
      return response;
    }

    // checking unique fields
    Hotel hotel = optionalItem.get();
    Search search = new Search(Hotel.class);

    if (hotelDto.getName() != null && !hotelDto.getName().isEmpty()) {
      search.addFilterEqual("name", hotelDto.getName());
      if (!hotel.equals(getHotelRepository().searchUnique(search))) {
        response.put("\"Hotel with name = " + hotelDto.getName() + " already exists\"", hotelDto);
        return response;
      }
    }

    if (hotelDto.getAddress() != null && !hotelDto.getAddress().isEmpty()) {
      search.addFilterEqual("address", hotelDto.getAddress());
      if (!hotel.equals(getHotelRepository().searchUnique(search))) {
        response.put("\"Hotel with address = " + hotelDto.getAddress() + " already exists\"", hotelDto);
        return response;
      }
    }

    if (hotelDto.getCityId() != null && hotelDto.getCityId() != 0) {
      Optional<City> optionalCity = getCityRepository().findById(hotelDto.getCityId());

      if (optionalCity.isEmpty()) {
        response.put("Hotel city not found", hotelDto);
        return response;
      }

      City city = optionalCity.get();
      hotel.setCity(city);
    }

    // update with values
    if (hotelDto.getName() != null) { hotel.setName(hotelDto.getName()); }
    if (hotelDto.isEnabled() != null) { hotel.setEnabled(hotelDto.isEnabled()); }
    if (hotelDto.getPosition() != null) { hotel.setPosition(hotelDto.getPosition()); }

    response.put("Hotel with id = " + id + " updated", updateDto(getHotelRepository().update(hotel), hotelDto));
    return response;
  }

  // Method delete

  public Map<String, HotelDto> delete(Long id) {
    Map<String, HotelDto> response = new HashMap<>();
    Optional<Hotel> optionalItem = getHotelRepository().findById(id);

    if (optionalItem.isEmpty()) {
      response.put("Hotel with id = " + id + " not found", new HotelDto());
      return response;
    }

    Hotel hotel = optionalItem.get();
    HotelDto hotelDto = createDto(hotel);
    getCityRepository().deleteById(id);
    response.put("Hotel with id = " + id + " deleted", hotelDto);
    return response;
  }

  // Method getByFilter

  public @Nonnull Map<String, List<HotelDto>> getByFilter(
      @Nonnull HotelFilterDto filterDto, @Nonnull Integer page, @Nonnull Integer size) {

    Map<String, List<HotelDto>> response = new HashMap<>();
    Search search = new Search(Hotel.class);

    // checking input parameters
    if (filterDto.getCityId() != null && filterDto.getCityId() != 0) {
      Optional<City> optionalCity = getCityRepository().findById(filterDto.getCityId());
      optionalCity.ifPresent(city -> search.addFilterEqual("city", optionalCity.get()));
    }

    if (filterDto.getName() != null && !filterDto.getName().equals("")) {
      search.addFilterEqual("name", filterDto.getName());
    }

    if (filterDto.getAddress() != null && !filterDto.getAddress().equals("")) {
      search.addFilterEqual("address", filterDto.getAddress());
    }

    if (filterDto.getPostalCode() != null && !filterDto.getPostalCode().equals("")) {
      search.addFilterEqual("postalCode", filterDto.getPostalCode());
    }

    if (filterDto.getHotelClass() != null && !filterDto.getHotelClass().equals("")) {
      search.addFilterEqual("hotelClass", filterDto.getHotelClass());
    }

    if (filterDto.getPrice() != null && filterDto.getPrice() != 0) {
      search.addFilterGreaterThan("price", filterDto.getPrice());
    }

    if (filterDto.isEnabled() != null) {
      search.addFilterEqual("enabled", filterDto.isEnabled());
    }

    if (filterDto.getSortFields() != null && !filterDto.getSortFields().isEmpty() && filterDto.getSortFields().size() > 0) {
      filterDto.getSortFields().forEach((k, v) -> search.addSort(new Sort(k, Direction.fromString(v))));
    }

    long count = getHotelRepository().count(search);

    // pagination
    if (size > 0) {
      search.setMaxResults(size);
      search.setPageNumber(page - 1);
    }

    // search
    List<Hotel> hotels = getHotelRepository().search(search);
    List<HotelDto> hotelDtos = new ArrayList<>();

    if (hotels.size() > 0) {
      for (Hotel hotel : hotels) {
        HotelDto hotelDto = new HotelDto();
        hotelDto.setId(hotel.getId());
        hotelDto.setCityId(hotel.getCity().getId());
        hotelDto.setName(hotel.getName());
        hotelDto.setAddress(hotel.getAddress());
        hotelDto.setPostalCode(hotel.getPostalCode());
        hotelDto.setHotelClass(hotel.getHotelClass());
        hotelDto.setPrice(hotel.getPrice());
        hotelDto.setEnabled(hotel.isEnabled());
        hotelDto.setPosition(hotel.getPosition());
        hotelDtos.add(hotelDto);
      }
      response.put("Retrieved list with " + count + " hotels", hotelDtos);
    } else {
      response.put("Retrieved empty list of hotels", hotelDtos);
    }
    return response;
  }

  private @Nonnull HotelDto createDto(@Nonnull Hotel hotel) {
    HotelDto hotelDto = new HotelDto();
    return updateDto(hotel, hotelDto);
  }

  private @Nonnull HotelDto updateDto(@Nonnull Hotel hotel, @Nonnull HotelDto hotelDto) {
    hotelDto.setId(hotel.getId());
    hotelDto.setCityId(hotel.getCity().getId());
    hotelDto.setName(hotel.getName());
    hotelDto.setAddress(hotel.getAddress());
    hotelDto.setPostalCode(hotel.getPostalCode());
    hotelDto.setHotelClass(hotel.getHotelClass());
    hotelDto.setPrice(hotel.getPrice());
    hotelDto.setEnabled(hotel.isEnabled());
    hotelDto.setPosition(hotel.getPosition());
    return hotelDto;
  }

  private @Nonnull Hotel createEntity(@Nonnull HotelDto hotelDto, @Nonnull City city) {
    Hotel hotel = new Hotel();
    return updateEntity(hotelDto, city, hotel);
  }

  private @Nonnull Hotel updateEntity(@Nonnull HotelDto hotelDto, @Nonnull City city, @Nonnull Hotel hotel) {
    hotel.setCity(city);
    hotel.setName(hotelDto.getName());
    hotel.setAddress(hotelDto.getAddress());
    hotel.setPostalCode(hotelDto.getPostalCode());
    hotel.setHotelClass(hotelDto.getHotelClass());
    hotel.setPrice(hotelDto.getPrice());
    hotel.setEnabled(hotelDto.isEnabled());
    hotel.setPosition(hotelDto.getPosition());
    return hotel;
  }

}
