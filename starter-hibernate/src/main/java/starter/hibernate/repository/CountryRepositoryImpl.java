package starter.hibernate.repository;

import io.xammax.jooby.hibernate.repository.SearchRepositoryImpl;
import starter.hibernate.model.entity.Country;

public class CountryRepositoryImpl extends SearchRepositoryImpl<Country, Long> implements CountryRepository { }
