package starter.hibernate.repository;

import io.xammax.jooby.hibernate.repository.SearchRepository;
import starter.hibernate.model.entity.City;

public interface CityRepository extends SearchRepository<City, Long> { }
