package starter.hibernate.repository;

import io.xammax.jooby.hibernate.repository.SearchRepository;
import starter.hibernate.model.entity.Country;

public interface CountryRepository extends SearchRepository<Country, Long> { }
