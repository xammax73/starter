package starter.hibernate.repository;

import io.xammax.jooby.hibernate.repository.SearchRepository;
import starter.hibernate.model.entity.Hotel;

public interface HotelRepository extends SearchRepository<Hotel, Long> { }
