package starter.hibernate.repository;

import io.xammax.jooby.hibernate.repository.SearchRepository;
import starter.hibernate.model.entity.Setting;

import javax.annotation.Nonnull;

public interface SettingRepository extends SearchRepository<Setting, Long> {

  @Nonnull String getFullSiteName();

  @Nonnull String getShortSiteName();

  @Nonnull String getMainDomain();

  @Nonnull Integer getDecimalPrecision();

  @Nonnull String getDecimalCommaSymbol();

  @Nonnull String getThousandsSeparator();

  @Nonnull Integer getMaxItemsPerPage();

  @Nonnull String getSortDirection();

}
