package starter.hibernate.repository;

import io.xammax.jooby.hibernate.repository.SearchRepositoryImpl;
import starter.hibernate.model.entity.City;

public class CityRepositoryImpl extends SearchRepositoryImpl<City, Long> implements CityRepository { }
