package starter.hibernate.repository;

import io.xammax.jooby.hibernate.repository.SearchRepositoryImpl;
import io.xammax.jooby.internal.hibernate.Search;
import starter.hibernate.model.entity.Setting;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Optional;

public class SettingRepositoryImpl extends SearchRepositoryImpl<Setting, Long> implements SettingRepository {

  /* System */
  private static final String DEFAULT_FULL_SITE_NAME              = "Hibernate starter";
  private static final String DEFAULT_SHORT_SITE_NAME             = "Starter";
  private static final String DEFAULT_MAIN_DOMAIN                 = "starter.loc";
  private static final Integer DEFAULT_DECIMAL_PRECISION          = 8;
  private static final String DEFAULT_DECIMAL_COMMA_SYMBOL        = ".";
  private static final String DEFAULT_THOUSANDS_SEPARATOR         = " ";
  //private static final Integer DEFAULT_PAGINATION_PAGE            = 1;
  private static final Integer DEFAULT_PAGINATION_SIZE            = 10;
  private static final String DEFAULT_SORT_DIRECTION              = "asc";

  // public

  public @Nonnull String getFullSiteName() {
    return getStringValue("system.full_site_name", DEFAULT_FULL_SITE_NAME);
  }

  public @Nonnull String getShortSiteName() {
    return getStringValue("system.short_site_name", DEFAULT_SHORT_SITE_NAME);
  }

  public @Nonnull String getMainDomain() {
    return getStringValue("system.main_domain", DEFAULT_MAIN_DOMAIN);
  }

  public @Nonnull Integer getDecimalPrecision() {
    return getIntValue("system.decimal_precision", DEFAULT_DECIMAL_PRECISION);
  }

  public @Nonnull String getDecimalCommaSymbol() {
    return getStringValue("system.decimal_comma_symbol", DEFAULT_DECIMAL_COMMA_SYMBOL);
  }

  public @Nonnull String getThousandsSeparator() {
    return getStringValue("system.thousands_separator", DEFAULT_THOUSANDS_SEPARATOR);
  }

  public @Nonnull Integer getMaxItemsPerPage() {
    return getIntValue("system.max_items_per_page", DEFAULT_PAGINATION_SIZE);
  }

  public @Nonnull String getSortDirection() {
    return getStringValue("system.sort_direction", DEFAULT_SORT_DIRECTION);
  }

  // private

  private @Nonnull Boolean getBooleanValue(String alias) {
    return Optional.of(getSearchSettings(alias))
        .map(items -> !items.get(0).getValue().isEmpty() && Boolean.parseBoolean(items.get(0).getValue()))
        .orElse(false);
  }

  private @Nonnull Double getDoubleValue(String alias, Double defaultValue) {
    return Optional.of(getSearchSettings(alias))
        .map(items -> Double.parseDouble(items.get(0).getValue()))
        .orElse(defaultValue);
  }

  private @Nonnull Integer getIntValue(String alias, Integer defaultValue) {
    return Optional.of(getSearchSettings(alias))
        .map(items -> Integer.parseInt(items.get(0).getValue()))
        .orElse(defaultValue);
  }

  private @Nonnull Long getLongValue(String alias, Long defaultValue) {
    return Optional.of(getSearchSettings(alias))
        .map(items -> Long.parseLong(items.get(0).getValue()))
        .orElse(defaultValue);
  }

  private @Nonnull String getStringValue(String alias, String defaultValue) {
    return Optional.of(getSearchSettings(alias))
        .map(items -> items.get(0).getValue())
        .orElse(defaultValue);
  }

  private List<Setting> getSearchSettings(String alias) {
    Search searchSettings = new Search(Setting.class);
    searchSettings.addFilterEqual("alias", alias);
    return this.search(searchSettings);
  }

}