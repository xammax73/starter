package starter.hibernate.repository;

import io.xammax.jooby.hibernate.repository.SearchRepositoryImpl;
import starter.hibernate.model.entity.Hotel;

public class HotelRepositoryImpl extends SearchRepositoryImpl<Hotel, Long> implements HotelRepository { }
