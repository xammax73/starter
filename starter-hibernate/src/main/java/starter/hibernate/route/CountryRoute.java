package starter.hibernate.route;

import io.jooby.Formdata;
import io.jooby.Jooby;
import io.jooby.json.JacksonModule;
import io.xammax.jooby.hibernate.TransactionalRequest;
import starter.hibernate.model.dto.CountryDto;
import starter.hibernate.service.CountryService;

import java.util.Optional;

public class CountryRoute extends Jooby {

  {
    JacksonModule jackson = new JacksonModule();

    /* Open session in view filter (entity manager + transaction): */
    decorator(new TransactionalRequest().enabledByDefault(true));

    get("/", ctx -> {
      CountryService service = require(CountryService.class);
      return service.getAll(
          !ctx.query("page").toOptional().equals(Optional.empty()) ? ctx.query("page").intValue() : null,
          !ctx.query("size").toOptional().equals(Optional.empty()) ? ctx.query("size").intValue() : null,
          !ctx.query("field").toOptional().equals(Optional.empty()) ? ctx.query("field").value() : null,
          !ctx.query("direction").toOptional().equals(Optional.empty()) ? ctx.query("direction").value() : null
      );
    });

    get("/{id:\\d+}", ctx -> {
      Long id = ctx.path("id").longValue();
      CountryService service = require(CountryService.class);
      return service.getById(id);
    });

    post("/", ctx -> {
      CountryService service = require(CountryService.class);
      CountryDto countryDto;

      if (ctx.body().bytes().length == 0) {
        Formdata form = ctx.form();
        countryDto = new CountryDto();
        countryDto.setCode(form.get("code").isMissing() ? null : form.get("code").value());
        countryDto.setName(form.get("name").isMissing() ? null : form.get("name").value());
        countryDto.setEnabled(form.get("enabled").isMissing() ? null : form.get("enabled").booleanValue());
        countryDto.setPosition(form.get("position").isMissing() ? null : form.get("position").longValue());
      }
      else {
        countryDto = (CountryDto) jackson.decode(ctx, CountryDto.class);
      }

      return service.create(countryDto);
    });

    put("/{id:\\d+}", ctx -> {
      Long id = ctx.path("id").longValue();
      CountryService service = require(CountryService.class);
      CountryDto countryDto;

      if (ctx.body().bytes().length == 0) {
        Formdata form = ctx.form();
        countryDto = new CountryDto();
        countryDto.setId(form.get("id").isMissing() ? null : id);
        countryDto.setCode(form.get("code").isMissing() ? null : form.get("code").value());
        countryDto.setName(form.get("name").isMissing() ? null : form.get("name").value());
        countryDto.setEnabled(form.get("enabled").isMissing() ? null : form.get("enabled").booleanValue());
        countryDto.setPosition(form.get("position").isMissing() ? null : form.get("position").longValue());
      }
      else {
        countryDto = (CountryDto) jackson.decode(ctx, CountryDto.class);
      }

      return service.update(countryDto, id);
    });

    delete("/{id:\\d+}", ctx -> {
      Long id = ctx.path("id").longValue();
      CountryService service = require(CountryService.class);
      return service.delete(id);
    });

  }

}
