package starter.hibernate.route;

import io.jooby.Formdata;
import io.jooby.Jooby;
import io.jooby.json.JacksonModule;
import io.xammax.jooby.hibernate.TransactionalRequest;
import starter.hibernate.model.dto.SettingDto;
import starter.hibernate.service.SettingService;

import java.util.Optional;

public class SettingRoute extends Jooby {

  {
    JacksonModule jackson = new JacksonModule();

    /* Open session in view filter (entity manager + transaction): */
    decorator(new TransactionalRequest().enabledByDefault(true));

    get("/", ctx -> {
      SettingService service = require(SettingService.class);
      return service.getAll(
          !ctx.query("page").toOptional().equals(Optional.empty()) ? ctx.query("page").intValue() : null,
          !ctx.query("size").toOptional().equals(Optional.empty()) ? ctx.query("size").intValue() : null,
          !ctx.query("field").toOptional().equals(Optional.empty()) ? ctx.query("field").value() : null,
          !ctx.query("direction").toOptional().equals(Optional.empty()) ? ctx.query("direction").value() : null
      );
    });

    get("/{id:\\d+}", ctx -> {
      Long id = ctx.path("id").longValue();
      SettingService service = require(SettingService.class);
      return service.getById(id);
    });

    put("/{id:\\d+}", ctx -> {
      Long id = ctx.path("id").longValue();
      SettingService service = require(SettingService.class);
      SettingDto settingDto;

      if (ctx.body().bytes().length == 0) {
        Formdata form = ctx.form();
        settingDto = new SettingDto();
        settingDto.setId(form.get("id").isMissing() ? null : id);
        settingDto.setName(form.get("name").isMissing() ? null : form.get("name").value());
        settingDto.setAlias(form.get("alias").isMissing() ? null : form.get("alias").value());
        settingDto.setDescription(form.get("description").isMissing() ? null : form.get("description").value());
        settingDto.setType(form.get("type").isMissing() ? null : form.get("type").value());
        settingDto.setValue(form.get("value").isMissing() ? null : form.get("value").value());
        settingDto.setPosition(form.get("position").isMissing() ? null : form.get("position").longValue());
      }
      else {
        settingDto = (SettingDto) jackson.decode(ctx, SettingDto.class);
      }

      return service.update(settingDto, id);
    });

    /* *
    delete("/{id:\\d+}", ctx -> {
      Long id = ctx.path("id").longValue();
      SettingService service = require(SettingService.class);
      return service.delete(id);
    });
     */

  }

}
