package starter.hibernate.route;

import io.jooby.Formdata;
import io.jooby.Jooby;
import io.jooby.json.JacksonModule;
import io.xammax.jooby.hibernate.TransactionalRequest;
import starter.hibernate.model.dto.CityDto;
import starter.hibernate.service.CityService;

import java.util.Optional;

public class CityRoute extends Jooby {

  {
    JacksonModule jackson = new JacksonModule();

    /* Open session in view filter (entity manager + transaction): */
    decorator(new TransactionalRequest().enabledByDefault(true));

    get("/", ctx -> {
      CityService service = require(CityService.class);
      return service.getAll(
          !ctx.query("page").toOptional().equals(Optional.empty()) ? ctx.query("page").intValue() : null,
          !ctx.query("size").toOptional().equals(Optional.empty()) ? ctx.query("size").intValue() : null,
          !ctx.query("field").toOptional().equals(Optional.empty()) ? ctx.query("field").value() : null,
          !ctx.query("direction").toOptional().equals(Optional.empty()) ? ctx.query("direction").value() : null
      );
    });

    get("/{id:\\d+}", ctx -> {
      Long id = ctx.path("id").longValue();
      CityService service = require(CityService.class);
      return service.getById(id);
    });

    post("/", ctx -> {
      CityService service = require(CityService.class);
      CityDto cityDto;

      if (ctx.body().bytes().length == 0) {
        Formdata form = ctx.form();
        cityDto = new CityDto();
        cityDto.setCountryId(form.get("countryId").isMissing() ? null : form.get("countryId").longValue());
        cityDto.setName(form.get("name").isMissing() ? null : form.get("name").value());
        cityDto.setEnabled(form.get("enabled").isMissing() ? null : form.get("enabled").booleanValue());
        cityDto.setPosition(form.get("position").isMissing() ? null : form.get("position").longValue());
      }
      else {
        cityDto = (CityDto) jackson.decode(ctx, CityDto.class);
      }

      return service.create(cityDto);
    });

    put("/{id:\\d+}", ctx -> {
      Long id = ctx.path("id").longValue();
      CityService service = require(CityService.class);
      CityDto cityDto;

      if (ctx.body().bytes().length == 0) {
        Formdata form = ctx.form();
        cityDto = new CityDto();
        cityDto.setId(form.get("id").isMissing() ? null : id);
        cityDto.setCountryId(form.get("countryId").isMissing() ? null : form.get("countryId").longValue());
        cityDto.setName(form.get("name").isMissing() ? null : form.get("name").value());
        cityDto.setEnabled(form.get("enabled").isMissing() ? null : form.get("enabled").booleanValue());
        cityDto.setPosition(form.get("position").isMissing() ? null : form.get("position").longValue());
      }
      else {
        cityDto = (CityDto) jackson.decode(ctx, CityDto.class);
      }

      return service.update(cityDto, id);
    });

    delete("/{id:\\d+}", ctx -> {
      Long id = ctx.path("id").longValue();
      CityService service = require(CityService.class);
      return service.delete(id);
    });
  }

}
