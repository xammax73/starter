package starter.hibernate.route;

import io.jooby.Formdata;
import io.jooby.Jooby;
import io.jooby.json.JacksonModule;
import io.xammax.jooby.hibernate.TransactionalRequest;
import starter.hibernate.model.dto.HotelDto;
import starter.hibernate.service.HotelService;

import java.util.Optional;

public class HotelRoute extends Jooby {

  {
    JacksonModule jackson = new JacksonModule();

    /* Open session in view filter (entity manager + transaction): */
    decorator(new TransactionalRequest());

    get("/", ctx -> {
      HotelService service = require(HotelService.class);
      return service.getAll(
          !ctx.query("page").toOptional().equals(Optional.empty()) ? ctx.query("page").intValue() : null,
          !ctx.query("size").toOptional().equals(Optional.empty()) ? ctx.query("size").intValue() : null,
          !ctx.query("field").toOptional().equals(Optional.empty()) ? ctx.query("field").value() : null,
          !ctx.query("direction").toOptional().equals(Optional.empty()) ? ctx.query("direction").value() : null
      );
    });

    get("/{id:\\d+}", ctx -> {
      Long id = ctx.path("id").longValue();
      HotelService service = require(HotelService.class);
      return service.getById(id);
    });

    post("/", ctx -> {
      HotelService service = require(HotelService.class);
      HotelDto hotelDto;

      if (ctx.body().bytes().length == 0) {
        Formdata form = ctx.form();
        hotelDto = new HotelDto();
        hotelDto.setCityId(form.get("cityId").isMissing() ? null : form.get("cityId").longValue());
        hotelDto.setName(form.get("name").isMissing() ? null : form.get("name").value());
        hotelDto.setAddress(form.get("address").isMissing() ? null : form.get("address").value());
        hotelDto.setPostalCode(form.get("postalCode").isMissing() ? null : form.get("postalCode").value());
        hotelDto.setHotelClass(form.get("hotelClass").isMissing() ? null : form.get("hotelClass").value());
        hotelDto.setPrice(form.get("price").isMissing() ? null : form.get("price").intValue());
        hotelDto.setEnabled(form.get("enabled").isMissing() ? null : form.get("enabled").booleanValue());
        hotelDto.setPosition(form.get("position").isMissing() ? null : form.get("position").longValue());
      }
      else {
        hotelDto = (HotelDto) jackson.decode(ctx, HotelDto.class);
      }

      return service.create(hotelDto);
    });

    put("/{id:\\d+}", ctx -> {
      Long id = ctx.path("id").longValue();
      HotelService service = require(HotelService.class);
      HotelDto hotelDto;

      if (ctx.body().bytes().length == 0) {
        Formdata form = ctx.form();
        hotelDto = new HotelDto();
        hotelDto.setId(form.get("id").isMissing() ? null : id);
        hotelDto.setCityId(form.get("cityId").isMissing() ? null : form.get("cityId").longValue());
        hotelDto.setName(form.get("name").isMissing() ? null : form.get("name").value());
        hotelDto.setAddress(form.get("address").isMissing() ? null : form.get("address").value());
        hotelDto.setPostalCode(form.get("postalCode").isMissing() ? null : form.get("postalCode").value());
        hotelDto.setHotelClass(form.get("hotelClass").isMissing() ? null : form.get("hotelClass").value());
        hotelDto.setPrice(form.get("price").isMissing() ? null : form.get("price").intValue());
        hotelDto.setEnabled(form.get("enabled").isMissing() ? null : form.get("enabled").booleanValue());
        hotelDto.setPosition(form.get("position").isMissing() ? null : form.get("position").longValue());
      }
      else {
        hotelDto = (HotelDto) jackson.decode(ctx, HotelDto.class);
      }

      return service.update(hotelDto, id);
    });

    delete("/{id:\\d+}", ctx -> {
      Long id = ctx.path("id").longValue();
      HotelService service = require(HotelService.class);
      return service.delete(id);
    });
  }

}
