insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(1, 'SoHo Inn', '82 Houston St', '10001', 'Diamond', 299, true, 1);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(1, 'NoHo Hotel', '99 Houston St', '10001', 'Gold', 199, true, 2);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(1, 'Broadway Suites', '887 Broadway', '10002', 'Gold', 119, true, 3);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(1, 'NY Convention Hotel', '288 6th Ave', '10002', 'Diamond', 299, true, 4);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(1, 'Central Park Suites', '900 Park Ave', '10003', 'Diamond', 219, true, 5);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(2, 'Downtown Inn', '528 Pico Blvd', '90012', 'Diamond', 199, true, 6);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(3, 'Grand Gold Resort', '123 Las Vegas Blvd', '89120', 'Platinum', 339, true, 7);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(3, 'Desert Sun Resort', '47 Las Vegas Blvd', '89119', 'Diamond', 105, true, 8);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(3, 'Casino World Resort', '28 Sunset Drive', '89120', 'Diamond', 211, true, 9);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(4, 'The Bay Hotel', '1 Fishermans Rd', '94105', 'Diamond', 299, true, 10);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(4, 'Golden Suites', '14 Golden Gate Blvd', '94014', 'Diamond', 324, true, 11);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(4, 'Metropolitan Hotel', '378 Green St', '94102', 'Platinum', 349, true, 12);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(5, 'Metropolis Hotel', '822 Edgware Rd', 'W2 4AD', 'Diamond', 379, true, 13);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(5, 'Royal Grande Hotel', '99 Garden Park', 'W2 3JP', 'Platinum', 449, true, 14);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(5, 'Palace Hotel', '537 Southwark', 'SE1 9HH', 'Platinum', 389, true, 15);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(6, 'Jurys Inn Birmingham', '245 Broad St', 'B1 2HQ', 'Platinum', 279, true, 16);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(7, 'Hillyard Mews', '20 Bryansford Road', 'BT33 0HJ', 'Platinum', 249, true, 17);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(8, 'Hamburg Suites', 'An Der Alster 82', '20099', 'Diamond', 299, true, 18);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(8, 'The Stanadard Resort', 'Steindamm 9', '20359', 'Diamond', 299, true, 19);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(8, 'The Park Hotel', 'Borstelmannsweg 82', '20537', 'Gold', 289, true, 20);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(9, 'Hotel Muenchen Palace', 'Trogerstrasse 21', '81675', 'Diamond', 329, true, 21);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(9, 'Platzl Hotel', 'Sparkassenstrasse 10', '80331', 'Diamond', 449, true, 22);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(10, 'Hotel de Paris', '49 Rue Pierre Charron', '75008', 'Platinum', 399, true, 23);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(10, 'Champs Elysees Hotel', '1 Champs Elysees', '75008', 'Platinum', 419, true, 24);

insert into starter_hotels (city_id, name, address, postal_code, hotel_class, price, enabled, position)
values(10, 'Opera Hotel', '77 Blvd Haussmann', '75009', 'Diamond', 349, true, 25);