insert into starter_cities (country_id, name, enabled, position)
values(1, 'New York', true, 1);

insert into starter_cities (country_id, name, enabled, position)
values(1, 'Los Angeles', true, 2);

insert into starter_cities (country_id, name, enabled, position)
values(1, 'Las Vegas', false, 3);

insert into starter_cities (country_id, name, enabled, position)
values(1, 'San Francisco', true, 4);

insert into starter_cities (country_id, name, enabled, position)
values(2, 'London', true, 5);

insert into starter_cities (country_id, name, enabled, position)
values(2, 'Birmingham', true, 6);

insert into starter_cities (country_id, name, enabled, position)
values(2, 'Newcastle', true, 7);

insert into starter_cities (country_id, name, enabled, position)
values(3, 'Hamburg', true, 8);

insert into starter_cities (country_id, name, enabled, position)
values(3, 'Munich', true, 9);

insert into starter_cities (country_id, name, enabled, position)
values(4, 'Paris', true, 10);