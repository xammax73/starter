insert into starter_settings (alias, name, description, type, value, position)
values('system.full_site_name', 'full site name', 'Full site name', 'String', 'Hibernate starter', 1);

insert into starter_settings (alias, name, description, type, value, position)
values('system.short_site_name', 'short site name', 'Short site name', 'String', 'Starter', 2);

insert into starter_settings (alias, name, description, type, value, position)
values('system.main_domain', 'main domain', 'Main domain', 'String', 'starter.loc', 3);

insert into starter_settings (alias, name, description, type, value, position)
values('system.decimal_precision', 'decimal precision', 'A number of symbols after comma for decimal type', 'Integer', '8', 4);

insert into starter_settings (alias, name, description, type, value, position)
values('system.decimal_comma_symbol', 'decimal comma symbol', 'A symbol for separating the integer and fractional parts', 'String', '.', 5);

insert into starter_settings (alias, name, description, type, value, position)
values('system.thousands_separator', 'thousands separator', 'Thousands separator in decimal number', 'String', ' ', 6);

insert into starter_settings (alias, name, description, type, value, position)
values('system.max_items_per_page', 'max items per page', 'The maximal value of items per page', 'Integer', '10', 7);

insert into starter_settings (alias, name, description, type, value, position)
values('system.default_sort_direction', 'default sort direction', 'Default sort items direction', 'String', 'asc', 8);
