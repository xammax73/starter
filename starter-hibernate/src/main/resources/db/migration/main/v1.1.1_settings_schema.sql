create table starter_settings
(
  id          bigint generated by default as identity
    constraint pk_starter_settings
      primary key,
  name        varchar(255) not null
    constraint uk_starter_settings_name_idx
      unique,
  alias       varchar(255) not null
    constraint uk_starter_settings_alias_idx
      unique,
  description varchar(255) not null,
  type        varchar(255) not null,
  value       varchar(255) not null,
  position    bigint       not null
);

alter table starter_settings
  owner to xammax;
