insert into starter_countries (code, name, enabled, position)
values('US', 'United States', true, 1);

insert into starter_countries (code, name, enabled, position)
values('EN', 'England', true, 2);

insert into starter_countries (code, name, enabled, position)
values('DE', 'Germany', true, 3);

insert into starter_countries (code, name, enabled, position)
values('FR', 'France', true, 4);

-- insert into starter_countries (code, name, enabled, position)
-- values('IT', 'Italy', true, 5);
